---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "朝雨露"
  text: "cyl's blogs"
  tagline: My great project tagline
  actions:
    - theme: brand
      text: Markdown Examples
    - theme: alt
      text: API Examples

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---
